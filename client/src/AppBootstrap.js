import { BrowserRouter as Router } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { TokenProvider } from "./components/TokenProvider";
import { K8sProvider } from "./components/K8sProvider";
import { App } from "./components/App";
import { IssuesProvider } from "./components/issues/IssuesProvider";
import { UserProvider } from "./components/UserProvider";
import './App.scss';
import { createContext, useContext } from "react";
import { ErrorBoundary } from "./components/ErrorBoundary";

function AppBootstrap({ config }) {
  return (
    <ErrorBoundary>
      <AppConfigContext.Provider value={ config }>
        <TokenProvider config={config}>
          <Router>
            <UserProvider authRequired={config.authRequired}>
              <K8sProvider>
                <IssuesProvider>
                  <ToastContainer position="bottom-right" />
                  <App />
                </IssuesProvider>
              </K8sProvider>
            </UserProvider>
          </Router>
        </TokenProvider>
      </AppConfigContext.Provider>
    </ErrorBoundary>
  );
}

export default AppBootstrap;

const AppConfigContext = createContext(null);

export const useAppConfig = () => {
  return useContext(AppConfigContext);
}