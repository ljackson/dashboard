import { useCallback, useEffect, useState } from "react";
import { useRouteMatch, Redirect } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import { useK8sData } from "../K8sProvider";
import "./Header.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { AuthDisplay } from "./AuthDisplay";
import { ConfigureCliButton } from "./ConfigureCliButton";
import { useUser } from "../UserProvider";
import { SealedSecretButton } from "./SealedSecretButton";

export const Header = () => {
  const { connected, namespaces, activeNamespace, setActiveNamespace } = useK8sData();
  const { user } = useUser();
  const match = useRouteMatch("/:namespace");
  const [newUrl, setNewUrl] = useState(null);

  useEffect(() => {
    if (match && match.params.namespace !== activeNamespace)
      setActiveNamespace(match.params.namespace);
  }, [match, activeNamespace, setActiveNamespace]);

  useEffect(() => {
    if (match && newUrl === match.url)
      setNewUrl(null);
  }, [newUrl, match]);

  const onChange = useCallback((newNamespace) => {
    const newUrl = match.url.replace(match.params.namespace, newNamespace);
    setNewUrl(newUrl);
    setActiveNamespace(newNamespace);
  }, [match, setActiveNamespace]);

  if (newUrl)
    return <Redirect to={newUrl} />;

  return (
    <Navbar variant="dark" className="mb-5" id="top-nav-bar" expand="lg">
      <Container>
        <Navbar.Brand>
          <img alt="Virginia Tech logo" src="/assets/logo.svg" />
          Platform Dashboard
        </Navbar.Brand>
        <Navbar.Text className="pe-3">
          { activeNamespace && (
            <ConnectionIndicator connected={connected} />
          )}
        </Navbar.Text>
        <Navbar.Toggle aria-controls="main-menu-nav" />
        <Navbar.Collapse id="main-menu-nav" className="justify-content-end">
          <Nav>
            { activeNamespace && (
              <SealedSecretButton activeNamespace={activeNamespace} />
            )}
            { user && (
              <ConfigureCliButton />
            )}
            <NavDropdown title={activeNamespace ? activeNamespace : "Namespaces"} onSelect={onChange} align="end">
              { namespaces.map(n => (
                <NavDropdown.Item key={n.metadata.name} eventKey={n.metadata.name} href="#">{ n.metadata.name }</NavDropdown.Item>
              ))}
            </NavDropdown>
            { user && (
              <AuthDisplay />
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

const ConnectionIndicator = ({ connected }) => {
  if (connected)
    return null;

  return (
    <>
      <FontAwesomeIcon icon={faSpinner} spin />
      &nbsp;
      Reconnecting...
    </>
  )
};