import * as React from "react";
import { toast } from "react-toastify";
import ReconnectingWebSocket from 'reconnecting-websocket';
import { useAuthToken } from "./TokenProvider";
import pako from "pako";
const { useMemo, useState, useEffect, useCallback } = React;


const K8sContext = React.createContext(null);
const PodMetricsContext = React.createContext(null);

const base64ToArrayBuffer = (base64) => {
  var binary_string = window.atob(base64);
  var len = binary_string.length;
  var bytes = new Uint8Array(len);
  for (let i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
  }
  return bytes.buffer;
}

const fetchData = (url, setter, errorHandler, token) => {
  const options = (token) ? {
    headers: new Headers({
      Authorization: `Bearer ${token}`
    })
  }: {};

  fetch(url, options)
    .then(r => Promise.all([r.status, r.json()]))
    .then(([status, data]) => {
      if (Math.floor(status / 100) !== 2) {
        const message = `${status} - ${data.message}`;
        toast.error(message);
        errorHandler(true);
      }
      else {
        setter(data.items);
        errorHandler(false);
      }
    })
}

const createInterval = (url, setter, period, errorHandler, token) => {
  fetchData(url, setter, errorHandler, token);
  const interval = setInterval(() => {
    fetchData(url, setter, errorHandler, token);
  }, period);
  return () => clearInterval(interval);
}

export const K8sProvider = ({children}) => {
  const token = useAuthToken();

  const [connected, setConnected] = useState(false);
  const [namespaces, setNamespaces] = useState(null);
  const [activeNamespace, setActiveNamespace] = useState(null);
  const [pods, setPods] = useState(null);
  const [podMetrics, setPodMetrics] = useState(null);
  const [deployments, setDeployments] = React.useState(null);
  const [certificates, setCertificates] = useState(null);
  const [ingresses, setIngresses] = useState(null);
  const [services, setServices] = useState(null);
  const [endpoints, setEndpoints] = useState(null);
  const [kustomizations, setKustomizations] = useState(null);
  const [helmReleases, setHelmReleases] = useState(null);
  const [hasFetchError, setHasFetchError] = useState(false);

  const getSetter = useCallback((kind) => {
    switch (kind) {
      case "Service":
        return setServices;
      case "Endpoints":
        return setEndpoints;
      case "Pod":
        return setPods;
      case "Deployment":
        return setDeployments;
      case "Certificate":
        return setCertificates;
      case "Ingress":
        return setIngresses;
      case "Kustomization":
        return setKustomizations;
      case "HelmRelease":
        return setHelmReleases;
      default:
        throw new Error("Unknown kind: " + kind);
    }
  }, []);

  useEffect(() => {
    if (!activeNamespace) return;
    const url = window.location.origin.replace("http", "ws");
    const rws = new ReconnectingWebSocket(`${url}/api/namespaces/${activeNamespace}/events`);
    let pingInterval = null;

    rws.addEventListener("message", (message) => {
      const data = JSON.parse(pako.inflate(base64ToArrayBuffer(message.data), {to: "string"}));
      const {event, type} = data;
      if (type !== "event")
        return;

      if (event.type === "SET") {
        Object.keys(event.data)
          .forEach((objectKind) => {
            const setter = getSetter(objectKind);
            setter(event.data[objectKind]);
          });
        return;
      }

      const setter = getSetter(event.object.kind);
      setter((currentData = []) => {
        const existingData = (currentData) ? currentData : [];
        if (event.type === "ADDED" || event.type === "MODIFIED") {
          const index = existingData.findIndex(d => d.metadata.name === event.object.metadata.name);
          if (index === -1) return [...existingData, event.object];

          // Determine if a change actually occurred
          if (existingData[index].metadata.resourceVersion === event.object.metadata.resourceVersion)
            return existingData;

            return [...existingData.slice(0, index), event.object, ...existingData.slice(index + 1)];
        }
        if (event.type === "DELETED") {
          const index = existingData.findIndex(d => d.metadata.name === event.object.metadata.name);
          if (index > -1)
            return [...existingData.slice(0, index), ...existingData.slice(index + 1)];
        }
      });      
    });

    rws.addEventListener("open", () => {
      setConnected(true);
      rws.send(JSON.stringify({ token }));
      console.log("WebSocket opened");

      pingInterval = setInterval(() => rws.send(JSON.stringify({ type: "heartbeat" })), 5000);
    });

    rws.addEventListener("close", () => {
      setConnected(false);
      console.log("WebSocket closed");
      
      if (pingInterval) {
        clearInterval(pingInterval);
        pingInterval = null;
      }
    });

    return () => rws.close();
  }, [activeNamespace, token, getSetter]);
  
  // Query namespaces
  useEffect(() => {
    return createInterval("/api/namespaces", setNamespaces, 120000, setHasFetchError, token);
  }, [token]);

  // Set pod metrics
  useEffect(() => {
    if (hasFetchError) return;
    
    setPodMetrics(null);
    if (activeNamespace)
      return createInterval(`/api/namespaces/${activeNamespace}/pod-metrics`, setPodMetrics, 10000, setHasFetchError, token);
  }, [activeNamespace, hasFetchError, token]);

  const getServiceAccounts = useCallback((setter) => {
    return fetchData(`/api/namespaces/${activeNamespace}/service-accounts`, setter, () => {}, token);
  }, [activeNamespace, token]);

  useEffect(() => {
    setPods(null);
    setPodMetrics(null);
    setDeployments(null);
    setCertificates(null);
    setIngresses(null);
    setServices(null);
    setEndpoints(null);
    setKustomizations(null);
  }, [activeNamespace]);

  const k8sContext = useMemo(() => ({
    connected,
    namespaces,
    activeNamespace, 
    setActiveNamespace,
    pods, 
    deployments, 
    services, 
    endpoints,
    ingresses, 
    certificates, 
    kustomizations,
    hasFetchError,
    getServiceAccounts,
  }), [connected, namespaces, activeNamespace, setActiveNamespace, pods, deployments, services, endpoints, ingresses, certificates, kustomizations, hasFetchError, getServiceAccounts]);

  const metrics = useMemo(() => ({
    metrics: podMetrics,
  }), [podMetrics]);

  return (
    <K8sContext.Provider value={ k8sContext }>
      <PodMetricsContext.Provider value={metrics}>
        { children }
      </PodMetricsContext.Provider>
    </K8sContext.Provider>
  );
};

export const useK8sData = () => {
  return React.useContext(K8sContext);
};

export const usePodMetrics = () => {
  return React.useContext(PodMetricsContext);
}
