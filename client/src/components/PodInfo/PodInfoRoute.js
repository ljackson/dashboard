import { useMemo } from "react";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { Link } from "react-router-dom"
import { useParams } from "react-router"
import { useK8sData } from "../K8sProvider";
import { NotFound } from "../NotFound";
import { PodDetailCard } from "./PodDetailCard";
import { PodContainerCard } from "./PodContainerCard";
import { useK8sIssues } from "../issues/IssuesProvider";


export const PodInfoRoute = () => {
  const issues = useK8sIssues();
  const { podIdentifier } = useParams();
  const { activeNamespace, pods } = useK8sData();

  const pod = useMemo(() => {
    if (!pods) return null;
    return pods.find(p => p.metadata.name === podIdentifier);
  }, [pods, podIdentifier]);

  const podIssues = useMemo(() => {
    if (!pod || !issues) return null;
    return issues.filter(issue => issue.references.find(r => r.metadata.name === pod.metadata.name) !== undefined);
  }, [pod, issues]);

  if (!pod)
    return <NotFound />;
  
  return (
    <Container>
      <Row>
        <Col>
          <Breadcrumb>
            <Breadcrumb.Item linkAs={Link} linkProps={{ to: `/${activeNamespace}` }}>{activeNamespace}</Breadcrumb.Item>
            <Breadcrumb.Item>Pods</Breadcrumb.Item>
            <Breadcrumb.Item active>{ podIdentifier }</Breadcrumb.Item>
          </Breadcrumb>
        </Col>
      </Row>
      <Row>
        <Col>
          <h2>{ pod.metadata.name }</h2>
          <PodDetailCard pod={pod} issues={podIssues} />
          { pod.spec.containers.map((container) => (
            <PodContainerCard key={container.name} pod={pod} container={container} />
          ))}
        </Col>
      </Row>
    </Container>
  )
}