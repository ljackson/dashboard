import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Link } from "react-router-dom";

export const WelcomePage = ({ namespaces, setActiveNamespace }) => {

  return (
    <Container>
      <Row>
        <Col>
          <h2>Welcome!</h2>
          <p>To get started, choose a tenant namespace below!</p>
          <ul>
            {namespaces.map((namespace) => (
              <li key={namespace.metadata.name}>
                <Link to={`/${namespace.metadata.name}`}>{namespace.metadata.name}</Link>
              </li> 
            ))}
          </ul>
        </Col>
      </Row>
    </Container>
  );
};
