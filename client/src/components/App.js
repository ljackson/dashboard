import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Route, Switch } from "react-router";
import { Dashboard } from "./Dashboard/Dashboard";
import { Header } from "./Header/Header";
import { IngressRoute } from "./Ingresses/IngressRoute";
import { useK8sData } from "./K8sProvider"
import { LoggedOutPage } from "./LoggedOutPage";
import { NotFound } from "./NotFound";
import { PodInfoRoute } from "./PodInfo/PodInfoRoute";
import { useUser } from "./UserProvider";
import { WelcomePage } from "./WelcomePage";
import { Footer } from "./Footer";
import { SealedSecretsRoute } from "./SealedSecrets/SealedSecretsRoute";

export const App = () => {
  const { namespaces, hasFetchError, setActiveNamespace } = useK8sData();
  const { loggedOut } = useUser();

  if (!namespaces) {
    return hasFetchError ? 
      <p>Authentication required before using this app</p> :
      <StartingUp />;
  }

  if (loggedOut) {
    return <LoggedOutPage />;
  }

  return (
    <>
      <Header />

      <Switch>
        <Route path="/" exact>
          <WelcomePage namespaces={namespaces} setActiveNamespace={setActiveNamespace} />
        </Route>
        <Route path="/:namespace" exact>
          <Dashboard />
        </Route>
        <Route path="/:namespace/sealed-secrets" exact>
          <SealedSecretsRoute />
        </Route>
        <Route path="/:namespace/ingresses/:ingressName" exact>
          <IngressRoute />
        </Route>
        <Route path="/:namespace/pods/:podIdentifier">
          <PodInfoRoute />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>

      <Footer />
    </>
  );
}

const StartingUp = () => {
  return (
    <div className="text-center text-muted mt-5">
      <FontAwesomeIcon icon={faSpinner} spin size="4x" className="mb-2" />
      <p>Starting up...</p>
    </div>
  )
};