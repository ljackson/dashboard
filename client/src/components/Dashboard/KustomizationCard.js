import { faCheckCircle, faExternalLinkAlt, faQuestionCircle, faSpinner, faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as React from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { LoadingSpinner } from "../LoadingSpinner";

export const KustomizationCard = ({ activeNamespace, kustomizations }) => {
  const kustomization = (kustomizations) ? kustomizations[0] : null;

  const status = React.useMemo(() => {
    if (!kustomization) {
      return {
        bg: "warning",
        text: "dark",
        icon: faQuestionCircle,
        heading: "No Kustomization found",
        subheading: "This is a platform issue, not yours!"
      };
    }

    const reason = kustomization.status.conditions[0].reason;
    switch (reason) {
      case "ReconciliationSucceeded":
        return { 
          bg: "success", 
          text: "light", 
          icon: faCheckCircle,
          heading: "Latest changes have been applied!",
          subheading: kustomization.status.lastAttemptedRevision,
        };
      case "ReconciliationFailed":
      case "BuildFailed":
        return { 
          bg: "danger", 
          text: "light", 
          icon: faTimesCircle,
          heading: "Latest changes failed to be applied",
          subheading: `Attempted: ${kustomization.status.lastAttemptedRevision}`
        };
      case "Progressing":
      case "DependencyNotReady":
        return { 
          bg: "info", 
          text: "light", 
          icon: faSpinner,
          heading: "Applying updates...",
          spin: true,
        };
      default:
        return { bg: "primary", text: "light", icon: faQuestionCircle };
    }
  }, [kustomization]);


  return (
    <Card bg={status.bg} text={status.text}>
      <Card.Body>
        { kustomizations ? (
          <>
            <Row>
              <Col md="2" className="text-center align-self-center">
                <FontAwesomeIcon icon={status.icon} size="3x" spin={status.spin} />
              </Col>
              <Col md="10">
                <h5>{ status.heading }</h5>
                { status.subheading && (
                  <div className="text-truncate">
                    <small>{ status.subheading }</small>
                  </div>
                )}
                <a href={`https://code.vt.edu/it-common-platform/cluster/tenants/${activeNamespace}`} className="text-white" target="_blank" rel="noreferrer">
                  Open manifest repo&nbsp;<FontAwesomeIcon icon={faExternalLinkAlt} />
                </a>
              </Col>
            </Row>
          </>
        ) : (
          <div className="text-center">
            <LoadingSpinner text="" />
          </div>
        )}
      </Card.Body>
    </Card>
  );
};