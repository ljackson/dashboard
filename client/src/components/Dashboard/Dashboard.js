import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { useK8sData, usePodMetrics } from "../K8sProvider";
import { PodCard } from "./PodCard";
import { CertificateCard } from "./CertificateCard";
import { DomainsCard } from "./DomainsCard";
import { KustomizationCard } from "./KustomizationCard";
import { IssueOverviewCards } from "./IssueOverviewCards";
import { LoadingSpinner } from "../LoadingSpinner";

export const Dashboard = () => {
  const { activeNamespace, certificates, ingresses, services, pods, kustomizations, endpoints } = useK8sData();
  const { metrics } = usePodMetrics();

  if (!pods) {
    return (
      <Container>
        <Row>
          <Col className="text-center">
            <LoadingSpinner text="Loading tenant data..." big twoColumn />
          </Col>
        </Row>
      </Container>
    );
  }

  return (
    <Container>
      <Row className="mb-3 row-eq-height">
        <Col sm={12} md={6} xl={5} className="mb-3">
          <KustomizationCard activeNamespace={activeNamespace} kustomizations={kustomizations} />
        </Col>
        <Col sm={12} md={6} xl={{ offset: 1, span: 6 }}>
          <IssueOverviewCards />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <DomainsCard activeNamespace={activeNamespace} ingresses={ingresses} services={services} pods={pods} endpoints={endpoints} />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <hr />
          <PodCard pods={pods} podMetrics={metrics} />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <CertificateCard certificates={certificates} />
        </Col>
      </Row>
    </Container>
  );
};