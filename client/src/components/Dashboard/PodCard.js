import { useMemo } from "react";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import { PodStatus } from "../helpers/PodStatus";
import { LoadingSpinner } from "../LoadingSpinner";
import { Link } from "react-router-dom";
import { useK8sData } from "../K8sProvider";
import { AgeDisplay } from "../helpers/AgeDisplay";
import { InputFilter, TableFilter } from "../Filters";
import { useTableContext, TableContextProvider } from "../context/TableContext";

export const PodCard = ({ pods, podMetrics }) => {
  return (
    <TableContextProvider data={pods} total={pods.length}>
    <Card>
      <Card.Header className="bg-secondary text-white">Pods</Card.Header>
      <Card.Body>
          < PodFilter />
        {(pods && podMetrics) ?
          <PodTable podMetrics={podMetrics} />
          : <LoadingSpinner text="Loading pods..." />
        }
      </Card.Body>
      </Card>
    </TableContextProvider>
  )
};

const PodTable = (props) => {
  const { data, filteredData, filteredTotal, total } = useTableContext();

  const orderedPods = useMemo(() => {
    if (!filteredData) return null;
    return filteredData.sort((a, b) => {
      return a.metadata.name < b.metadata.name ? -1 : 1;
    });
  }, [filteredData]);

  if (data < 1) {
    return (
      <p>You have no pods defined yet!</p>
    );
  } else if (filteredTotal < 1) {
    return (
      <p>No pod names match the filter provided</p>
    );
  }

  return (
    <>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>Name</th>
            <th>Image</th>
            <th>Status</th>
            <th>Age</th>
          </tr>
        </thead>
        <tbody>
          {orderedPods.map(pod => <PodRow key={pod.metadata.name} pod={pod} />)}
        </tbody>
        <tfoot>
          <tr><td>{filteredTotal} of {total}</td></tr>
        </tfoot>
      </Table>
    </>
  );
}

const PodRow = ({ pod }) => {
  const { activeNamespace } = useK8sData();

  return (
    <tr>
      <td>
        <Link to={`/${activeNamespace}/pods/${pod.metadata.name}`}>
          { pod.metadata.name }
        </Link>
      </td>
      <td>{ pod.spec.containers[0].image }</td>
      <td><PodStatus pod={pod} /></td>
      <td><AgeDisplay date={pod.metadata.creationTimestamp} /></td>
    </tr>
  );
}

const PodFilter = () => {
  const podQuery = (pod, inputValue) => pod.metadata.name.includes(inputValue)

  return (<TableFilter displayAt={10}>
    <InputFilter name="pod-name" placeholder="Filter by pod name" query={podQuery} />
  </TableFilter>);
}