const compression = require("compression");
const express = require("express");
const morgan = require("morgan");
const path = require("path");
const helmet = require("helmet");
const config = require("./config");
const { addHealthCheck } = require("./healthcheck");
const { addMetrics } = require("./prometheus");
const { addSessionHandling } = require("./session");
const { configureAuth } = require("./auth");
const router = require("./router");
const websocket = require("./websocket");

const app = express();
require("express-ws")(app);

app.use(compression());

// Add CSP, frame protections, and more
app.use(helmet({
  contentSecurityPolicy: {
    useDefaults: true,
    directives: {
      defaultSrc: `'self' wss://${config.getHostName()}`,
    }
  }
}));

// Static HTML, JS, CSS resources that are bundled in the image
app.use(express.static(__dirname + "/client"));

// Add health check and prometheus metrics handling
addHealthCheck(app);
addMetrics(app);

// Configure the Redis session management
addSessionHandling(app);

// Add request logging (don't want healthchecks and metrics logged)
app.use(morgan("common"));

// Add auth to the API
configureAuth(app);

// Websocket and API router
app.ws("/api/namespaces/:namespace/events", websocket);
app.use("/api", router);

// Support the routes the SPA might have
app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname, "client", "index.html"));
});

app.listen(3000, () => console.log("Listening on port 3000"));
process.on("SIGINT", () => process.exit());
process.on("SIGTREM", () => process.exit());