const AsyncRouter = require("express-async-router").AsyncRouter;
const k8sService = require("./k8s/k8sService");
const k8sClient = require("./k8s/client");
const config = require("./config");
const fetch = require("node-fetch");

const router = new AsyncRouter();

let getIdentifier = (req) => {
  return "local";
}

let getAuthToken = (req) => {
  return undefined;
}

if (config.getClientMode() === "OAUTH") {
  const auth = require("./auth/oauth");
  getAuthToken = auth.getAuthToken;
  configHandler = auth.configHandler;
} else if (config.getClientMode() === "AUTH_HEADER") {
  const auth = require("./auth/authHeader");
  getAuthToken = auth.getAuthToken;
  configHandler = auth.configHandler;
}

// Will only succeed if authentication passes since the auth
// has already configured its middleware
router.get("/verify-auth", async (req, res) => {

  // Attempt to fetch something. If it fails, we might need to get a new token
  const client = k8sClient.getCoreApiClient(getIdentifier(req), getAuthToken(req));
  await client.listNamespace(undefined, undefined, undefined, undefined, "platform.it.vt.edu/purpose=platform-tenant");

  res.send({ success: true });
});

router.get("/sealed-secrets-cert.pem", async (req, res) => {
  const response = await fetch(config.getSealedSecretCertLocation());
  const body = await response.text();
  res.send(body);
});

router.get("/namespaces", async (req, res) => {
  const client = k8sClient.getCoreApiClient(getIdentifier(req), getAuthToken(req));
  const namespaceRequest = await client.listNamespace(undefined, undefined, undefined, undefined, "platform.it.vt.edu/purpose=platform-tenant");

  const items = [];
  for (let i = 0; i < namespaceRequest.body.items.length; i++) {
    const namespace = namespaceRequest.body.items[i];
    try { 
      await client.readNamespace(namespace.metadata.name); 
      items.push(namespace);
    } catch {}
  }

  res.send({ items });
});

router.get("/namespaces/:namespace/deployments", async (req, res) => {
  const deployments = await k8sService.getDeployments(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: deployments });
});

router.get("/namespaces/:namespace/pods", async (req, res) => {
  const pods = await k8sService.getPods(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: pods });
});

router.get("/namespaces/:namespace/pods/:podName/:containerName/logs", async (req, res) => {
  const podLogs = await k8sService.getPodLogs(
    getIdentifier(req), getAuthToken(req), req.params.namespace, req.params.podName, req.params.containerName, 500
  );
  res.send(podLogs);
});

router.get("/namespaces/:namespace/service-accounts", async (req, res) => {
  const serviceAccounts = await k8sService.getServiceAccounts(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: serviceAccounts });
});

router.get("/namespaces/:namespace/services", async (req, res) => {
  const services = await k8sService.getServiceAccounts(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: services });
});

router.get("/namespaces/:namespace/endpoints", async (req, res) => {
  const endpoints = await k8sService.getEndpoints(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: endpoints });
});

router.get("/namespaces/:namespace/kustomizations", async (req, res) => {
  const kustomizations = await k8sService.getKustomizations(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: kustomizations });
});

router.get("/namespaces/:namespace/helm-releases", async (req, res) => {
  const helmReleases = await k8sService.getHelmReleases(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: helmReleases });
});

router.get("/namespaces/:namespace/certificates", async (req, res) => {
  const certificates = await k8sService.getCertificates(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: certificates });
});

router.get("/namespaces/:namespace/jobs", async (req, res) => {
  const jobs = await k8sService.getJobs(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: jobs });
});

router.get("/namespaces/:namespace/cron-jobs", async (req, res) => {
  const cronJobs = await k8sService.getCronJobs(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: cronJobs });
});

router.get("/namespaces/:namespace/ingresses", async (req, res) => {
  const ingresses = await k8sService.getIngresses(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: ingresses });
});

router.get("/namespaces/:namespace/pod-metrics", async (req, res) => {
  const metrics = await k8sService.getPodMetrics(getIdentifier(req), getAuthToken(req), req.params.namespace);
  res.send({ items: metrics });
});

router.use((err, req, res, next) => {
  if (err.statusCode === 401 || err.status === 401)
    return res.status(401).send({ message: "Need to reauthenticate"});

  console.log(err);
  res.status(500).send({ message: "An unrecoverable server error has occurred" });
});

module.exports = router;