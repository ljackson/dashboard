function configureApp(app) {
  app.get("/api/whoami", (req, res) => {
    res.send({});
  });
}

const getAuthToken = (_) => {
  return null;
};

const configHandler = (req, res) => {
  res.send({
    authRequired: false,
  });
};

module.exports = {
  configureApp,
  getAuthToken,
  configHandler,
};