
function addHealthCheck(app) {
  app.use("/api/healthz", (req, res) => res.send({ success: true }));
}

module.exports = {
  addHealthCheck,
};