FROM node:lts-alpine AS base

FROM base AS clientBuild
WORKDIR /usr/local/app
COPY client/package.json client/yarn.lock ./
RUN yarn install
COPY client/public ./public
COPY client/src ./src
RUN yarn build

FROM base
WORKDIR /usr/local/app
COPY backend/package.json backend/yarn.lock ./
RUN yarn install --production
COPY backend/src ./src
COPY --from=clientBuild /usr/local/app/build /usr/local/app/src/client
ARG VERSION
ENV VERSION=${VERSION}
RUN echo "{\"version\":\"$VERSION\"}" > /usr/local/app/src/client/version.json
CMD ["node", "src/index.js"]
